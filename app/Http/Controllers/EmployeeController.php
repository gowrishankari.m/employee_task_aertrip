<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Employee;
use App\Models\Address;
use App\Models\Contact;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('search');
        $employees = Employee::where('name', 'like', '%' . $search . '%')->get();

        return view('employees.index', compact('employees', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('employees.create', compact('departments'));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'department_id' => 'required|exists:departments,id',
        ]);

        $employee = new Employee;
        $employee->name = $request->input('name');
        $employee->department_id = $request->input('department_id');
        $employee->save();

        if ($request->has('contact_numbers')) {
            foreach ($request->input('contact_numbers') as $number) {
                $employee->contacts()->create(['phone' => $number]);
            }
        }

        if ($request->has('addresses')) {
            foreach ($request->input('addresses') as $address) {
                $employee->addresses()->create(['address' => $address]);
            }
        }

        return redirect()->route('employees.index')->with('success', 'Employee added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::findOrFail($id);
        $employeeAddresses = Address::where('employee_id', $id)->get();

        return view('employees.show', compact('employee', 'employeeAddresses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::with(['contacts'])->findOrFail($id);
        $employeeAddresses = Address::where('employee_id', $id)->get();        
        $departments = Department::all();

        return view('employees.edit', compact('employee','employeeAddresses','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::findOrFail($id);

        $employee->update([
            'name' => $request->input('name'),
            'department_id' => $request->input('department_id'),
        ]);

        $contactNumbers = $request->input('contact_numbers', []);
        $employee->contacts()->delete();
        foreach ($contactNumbers as $number) {
            $employee->contacts()->create(['phone' => $number]);
        }

        $addresses = $request->input('addresses', []);
        $employee->addresses()->delete();
        foreach ($addresses as $address) {
            $employee->addresses()->create(['address' => $address]);
        }

        return redirect()->route('employees.index')->with('success', 'Employee updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::findOrFail($id);
        $employee->delete();

        $contact = Contact::where('employee_id', $id);
        $contact->delete();

        $contact = Address::where('employee_id', $id);
        $contact->delete();

        return redirect()->route('employees.index')->with('success', 'Employee deleted successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function confirmDelete($id)
    {
        $employee = Employee::findOrFail($id);
        return view('employees.confirm-delete', compact('employee'));
    }
}
