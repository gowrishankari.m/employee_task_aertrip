@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Add Employee</h1>

        <form action="{{ route('employees.store') }}" method="POST">
            @csrf
            
            <div class="mb-3">
                <label for="name" class="form-label">Name:</label>
                <input type="text" name="name" class="form-control" required>
            </div>

            <div class="mb-3">
                <label for="department_id" class="form-label">Department:</label>
                <select name="department_id" class="form-select">
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="mb-3">
                <label for="contact_numbers" class="form-label">Contact Numbers:</label>
                <div class="input-group">
                    <input type="text" name="contact_numbers[]" class="form-control" placeholder="Contact Number">
                    <div class="input-group-append" style="margin-left: 10px;">
                        <button type="button" class="btn btn-success" id="add-contact">Add</button>
                    </div>
                </div>
                <div id="contact-numbers-container"></div>
            </div>

            <div class="mb-3">
                <label for="addresses" class="form-label">Addresses:</label>
                <div class="input-group">
                    <input type="text" name="addresses[]" class="form-control" placeholder="Address">
                    <div class="input-group-append" style="margin-left: 10px;">
                        <button type="button" class="btn btn-success" id="add-address">Add</button>
                    </div>
                </div>
                <div id="addresses-container"></div>
            </div>            

            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Add Employee</button>
                <a href="{{ route('employees.index') }}" class="btn btn-secondary">Back to List</a>
            </div>
        </form>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $(document).ready(function () {
        $('#add-contact').click(function () {
            $('#contact-numbers-container').append('<div class="input-group mt-2"><input type="text" name="contact_numbers[]" class="form-control" placeholder="Contact Number"><button type="button" class="btn btn-danger remove-contact">Remove</button></div>');
        });

        $('#add-address').click(function () {
            $('#addresses-container').append('<div class="input-group mt-2"><input type="text" name="addresses[]" class="form-control" placeholder="Address"><button type="button" class="btn btn-danger remove-address">Remove</button></div>');
        });

        $('#contact-numbers-container').on('click', '.remove-contact', function () {
            $(this).parent().remove();
        });

        $('#addresses-container').on('click', '.remove-address', function () {
            $(this).parent().remove();
        });
    });
</script>