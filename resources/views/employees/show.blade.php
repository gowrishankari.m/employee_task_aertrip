@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Employee Details</h1>
        <p><strong>Name:</strong> {{ $employee->name }}</p>
        <p><strong>Department:</strong> {{ $employee->department->name }}</p>
        <p><strong>Contact Numbers:</strong></p>
        <ul>            
            @foreach ($employee->contacts ?? [] as $contactNumber)
                <li>{{ $contactNumber->phone }}</li>
            @endforeach
        </ul>

        <p><strong>Addresses:</strong></p>
        <ul>            
            @foreach ($employeeAddresses ?? [] as $address)
                <li>{{ $address->address }}</li>
            @endforeach
        </ul>

        <div class="mb-3">
            <a href="{{ route('employees.index') }}" class="btn btn-secondary">Back to List</a>
        </div>
    </div>
@endsection