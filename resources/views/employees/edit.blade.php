@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit Employee</h1>

        <form action="{{ route('employees.update', $employee->id) }}" method="POST">
            @csrf
            @method('PUT')

            <div class="mb-3">
                <label for="name" class="form-label">Name:</label>
                <input type="text" name="name" class="form-control" value="{{ $employee->name }}" required>
            </div>

            <div class="mb-3">
                <label for="department_id" class="form-label">Department:</label>
                <select name="department_id" class="form-select">
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}" {{ $employee->department_id == $department->id ? 'selected' : '' }}>
                            {{ $department->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <p><strong>Contact Numbers:</strong></p>
            @foreach ($employee->contacts as $key => $contactNumber)
                <div class="mb-3">
                    <label for="contact_numbers[{{ $key }}]" class="form-label">Contact Number {{ $key + 1 }}:</label>
                    <input type="text" name="contact_numbers[{{ $key }}]" class="form-control" value="{{ $contactNumber->phone }}">
                </div>
            @endforeach

            <p><strong>Addresses:</strong></p>
            @foreach ($employeeAddresses as $key => $address)
                <div class="mb-3">
                    <label for="addresses[{{ $key }}]" class="form-label">Address {{ $key + 1 }}:</label>
                    <input type="text" name="addresses[{{ $key }}]" class="form-control" value="{{ $address->address }}">
                </div>
            @endforeach

            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Update Employee</button>
                <a href="{{ route('employees.index') }}" class="btn btn-secondary">Back to List</a>
            </div>
        </form>
    </div>
@endsection
