@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Confirm Deletion</h1>
        <p>Are you sure you want to delete this employee?</p>
        <p><strong>Name:</strong> {{ $employee->name }}</p>
        <p><strong>Department:</strong> {{ $employee->department->name }}</p>

        <form action="{{ route('employees.destroy', $employee->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <div class="mb-3">
                <button type="submit" class="btn btn-danger">Delete</button>
                <a href="{{ route('employees.index') }}" class="btn btn-secondary">Cancel</a>
            </div>
        </form>
    </div>
@endsection
