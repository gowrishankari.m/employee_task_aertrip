@extends('layouts.app')

@section('content')
    <div class="dashboard">
        <h1 class="dashboard-title">Welcome to the Dashboard</h1>

        <div class="dashboard-button">
            <a href="{{ route('employees.index') }}" class="btn btn-primary">Manage Employees</a>
        </div>

        <div class="dashboard-button">
            <a href="{{ route('departments.index') }}" class="btn btn-primary">Manage Departments</a>
        </div>
    </div>
@endsection

<style>
    .dashboard {
        text-align: center;
        padding: 20px;
    }

    .dashboard-title {
        font-size: 24px;
        margin-bottom: 20px;
    }

    .dashboard-button {
        margin-bottom: 10px;
    }
</style>
