@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-4">Department Details</h2>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{ $departments->name }}</h5>
                    <a href="{{ route('departments.index') }}" class="btn btn-success mb-4">Return back</a>                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
