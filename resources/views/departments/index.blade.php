@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-4">Department List</h2>            
            <a href="{{ route('departments.create') }}" class="btn btn-success mb-4">Add Department</a>
            <table class="table table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>Name</th>                        
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($departments as $department)
                        <tr>
                            <td>{{ $department->name }}</td>
                            <td>
                                <a href="{{ route('departments.show', $department->id) }}" class="btn btn-info btn-sm mr-2">View</a>
                                <a href="{{ route('departments.edit', $department->id) }}" class="btn btn-primary btn-sm mr-2">Edit</a>
                                <form action="{{ route('departments.destroy', $department->id) }}" method="POST" style="display: inline;">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this department?')">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection