@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2 class="my-4">Add Department</h2>
            <form action="{{ route('departments.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>                
                <button type="submit" class="btn btn-primary">Add</button>
                <a href="{{ route('departments.index') }}" class="btn btn-success">Cancel</a>
            </form>
        </div>
    </div>
</div>
@endsection
