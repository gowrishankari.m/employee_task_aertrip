<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\DepartmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('employees', EmployeeController::class);

Route::get('employees/{employee}/confirm-delete', 'App\Http\Controllers\EmployeeController@confirmDelete')->name('employees.confirm-delete');

Route::resource('departments', DepartmentController::class);

Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name('dashboard');